package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import prestamo.desktop.model.UserModel;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;

public class MenuView extends JFrame implements View{

	private static MenuView instancia;
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnPrestamos;
	private JMenuItem mntmSolicitar;
	private JLabel lblB;
	
	public static MenuView getMenuView() {
		if(instancia == null) {
			instancia = new MenuView();
		}
		return instancia;
	}
	
	
	/**
	 * Create the frame.
	 */
	private MenuView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
	
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnPrestamos = new JMenu("Prestamos");
		menuBar.add(mnPrestamos);
		
		mntmSolicitar = new JMenuItem("Solicitar");
		mnPrestamos.add(mntmSolicitar);
		
		lblB = new JLabel("Bienvenido "+UserModel.getUserModel().getApellido()+" "+UserModel.getUserModel().getApellido());
		getContentPane().add(lblB, BorderLayout.CENTER);
	
	}
	
	public JMenuItem getMntmSolicitar() {
		return mntmSolicitar;
	}
	public void setMntmSolicitar(JMenuItem mntmSolicitar) {
		this.mntmSolicitar = mntmSolicitar;
	}
	
	
	
}
