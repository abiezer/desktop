package prestamo.desktop.view;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;

public class LoginView extends JFrame implements View{

	private JPanel contentPane;
	private JTextField userField;
	private JPasswordField passwordField;
	private JButton btnIniciarSesion;
	private String valorUser;
	

	/**
	 * Create the frame.
	 */
	public LoginView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		contentPane.setLayout(null);
		
		userField = new JTextField();
		userField.setBounds(167, 68, 195, 20);
		
		userField.setColumns(10);
		
		btnIniciarSesion = new JButton("Iniciar Sesion");
		btnIniciarSesion.setBounds(120, 149, 131, 23);
		
		
		passwordField = new JPasswordField();
		passwordField.setBounds(167, 99, 195, 20);
		
		
		JLabel lblCorreo = new JLabel("Correo");
		lblCorreo.setBounds(29, 71, 46, 14);
		
		
		JLabel lblContrasea = new JLabel("Contraseña");
		lblContrasea.setBounds(29, 102, 76, 14);
		
		
		contentPane.add(btnIniciarSesion);
		contentPane.add(passwordField);
		contentPane.add(lblCorreo);
		contentPane.add(lblContrasea);
		contentPane.add(userField);
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public JTextField getUserField() {
		return userField;
	}

	public void setUserField(JTextField userField) {
		this.userField = userField;
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}

	public void setPasswordField(JPasswordField passwordField) {
		this.passwordField = passwordField;
	}

	public JButton getBtnIniciarSesion() {
		return btnIniciarSesion;
	}
	
	public String getValorUser() {
		return userField.getText();
	}
	

	public void setBtnIniciarSesion(JButton btnIniciarSesion) {
		this.btnIniciarSesion = btnIniciarSesion;
	}
}
