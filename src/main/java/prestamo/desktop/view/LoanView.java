package prestamo.desktop.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import com.jgoodies.forms.factories.DefaultComponentFactory;


public class LoanView extends JFrame implements View{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static LoanView instancia;
	private JPanel contentPane;
	private JTextField amountField;
	private JComboBox<Object> quotaComboBox;
	private JButton btnRegister;
	private JButton btnModify;
	private JButton btnDelete;
	private JButton btnSearch;
	private JMenuItem mntmInicio;

	/**
	 * Create the frame.
	 */
	private LoanView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mntmInicio = new JMenuItem("Inicio");
		menuBar.add(mntmInicio);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 414, 215);
		contentPane.add(panel);
		panel.setLayout(null);
		
		btnRegister = new JButton("Registrar");
		btnRegister.setBounds(315, 11, 89, 23);
		panel.add(btnRegister);
				
		btnModify = new JButton("Modificar");
		btnModify.setBounds(315, 45, 89, 23);
		panel.add(btnModify);
		
		btnDelete = new JButton("Eliminar");
		btnDelete.setBounds(315, 79, 89, 23);
		panel.add(btnDelete);
		
		btnSearch = new JButton("Buscar");
		btnSearch.setBounds(315, 113, 89, 23);
		panel.add(btnSearch);
		
		
		amountField = new JTextField();
		amountField.setBounds(82, 80, 195, 20);
		panel.add(amountField);
		amountField.setColumns(10);
		
		quotaComboBox = new JComboBox<Object>();
		quotaComboBox.setModel(new DefaultComboBoxModel<Object>(new String[] {"6", "12", "24", "36", "48"}));
		quotaComboBox.setSelectedIndex(0);
		quotaComboBox.setBounds(82, 114, 195, 20);
		panel.add(quotaComboBox);
		
		JLabel lblPersona_1 = new JLabel("Monto");
		lblPersona_1.setBounds(10, 83, 46, 14);
		panel.add(lblPersona_1);
		
		JLabel lblCuotas = new JLabel("Cuotas");
		lblCuotas.setBounds(10, 117, 46, 14);
		panel.add(lblCuotas);
		
		JTextArea textArea = new JTextArea();
		textArea.setBounds(82, 10, 195, 57);
		panel.add(textArea);
		
		JLabel lblMotivo = DefaultComponentFactory.getInstance().createLabel("Motivo");
		lblMotivo.setBounds(10, 32, 46, 14);
		panel.add(lblMotivo);
		
		
	}
	
	public static LoanView getLoanView() {
		if(instancia==null) {
			instancia = new LoanView();
		}
		return instancia;
	}

	public JTextField getAmountField() {
		return amountField;
	}

	public void setAmountField(JTextField amountField) {
		this.amountField = amountField;
	}

	public JComboBox<Object> getQuotaComboBox() {
		return quotaComboBox;
	}

	public void setQuotaComboBox(JComboBox<Object> quotaComboBox) {
		this.quotaComboBox = quotaComboBox;
	}

	public JButton getBtnRegister() {
		return btnRegister;
	}

	public void setBtnRegister(JButton btnRegister) {
		this.btnRegister = btnRegister;
	}

	public JButton getBtnModify() {
		return btnModify;
	}

	public void setBtnModify(JButton btnModify) {
		this.btnModify = btnModify;
	}

	public JButton getBtnDelete() {
		return btnDelete;
	}

	public void setBtnDelete(JButton btnDelete) {
		this.btnDelete = btnDelete;
	}

	public JButton getBtnSearch() {
		return btnSearch;
	}

	public void setBtnSearch(JButton btnSearch) {
		this.btnSearch = btnSearch;
	}

	public JMenuItem getMntmInicio() {
		return mntmInicio;
	}

	public void setMntmInicio(JMenuItem mntmInicio) {
		this.mntmInicio = mntmInicio;
	}

	
}
