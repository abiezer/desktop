package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;

public class PrestamoForm extends JFrame implements View{

	private JPanel contentPane;
	private JTextField textField;
	private JButton btnGuardar;
	private JLabel lblMonto;


	/**
	 * Create the frame.
	 */
	public PrestamoForm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(192, 61, 137, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(141, 162, 89, 23);
		contentPane.add(btnGuardar);
		
		lblMonto = new JLabel("Monto");
		lblMonto.setBounds(58, 64, 46, 14);
		contentPane.add(lblMonto);
	}
}
