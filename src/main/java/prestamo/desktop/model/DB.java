package prestamo.desktop.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DB {

	private static String Url = "jdbc:sqlserver://"
			+ "52.54.212.137:1433;"
			+ "databaseName=abiezerSifontesPrestamos;"
			+ "user=abiezer;"
			+ "password=1234";
	/**
	 * Este Metodo Retorna un objeto de tipo conexion la base de datos abiezerSifontesPrestamos 
	 * */
	public static Connection getConnection(){
		
		Connection con = null;
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			con = DriverManager.getConnection(Url);
			return con;
		} catch (SQLException | ClassNotFoundException e) {
			System.out.println(e.getMessage().toString());
		}
		return con;
	}

}
