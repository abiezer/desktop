package prestamo.desktop.model;

public interface Model {
	public abstract boolean create();
	public abstract boolean update();
	public abstract boolean delete();
	public abstract boolean get();
}
