package prestamo.desktop.controller;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

import prestamo.desktop.model.LoanModel;
import prestamo.desktop.model.RegionModel;
import prestamo.desktop.model.UserModel;
import prestamo.desktop.view.LoanView;
import prestamo.desktop.view.LoginView;
import prestamo.desktop.view.UserUpdateView;

public class Main {
	
	public static void main(String[] args) {
//		LoginView frame = new LoginView();
//		UserModel loanModel = UserModel.getUserModel();
//		
//		LoginController loanCtrl = new LoginController(frame,loanModel);
//		frame.setVisible(true);
		
		UserUpdateView frame = new UserUpdateView();
		RegionModel rm = new RegionModel();
		
		ArrayList<RegionModel> regiones = rm.getList();
		
		int cantidadRegiones = regiones.size();
		
		String [] regionesString = new String[cantidadRegiones];
		
		for(int i = 0; i<cantidadRegiones;i++) {
			regionesString[i] = regiones.get(i).getNombre();
		}
		
		frame.comboBox.setModel(new DefaultComboBoxModel(regionesString));
		frame.comboBox = new JComboBox();
		frame.comboBox.setBounds(173, 149, 232, 20);
		frame.contentPane.add(frame.comboBox);
		frame.setVisible(true);
	}
}
