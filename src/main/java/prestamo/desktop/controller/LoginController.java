package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import prestamo.desktop.model.UserModel;
import prestamo.desktop.view.LoginView;
import prestamo.desktop.view.MenuView;

public class LoginController implements ActionListener{
	
	LoginView view = null;
	UserModel model = null;

	LoginController(LoginView view,UserModel model){
			this.view = view;
			this.model = model;
			view.getBtnIniciarSesion().addActionListener(this);
		
	}
	
	public void autenticate(){
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		if(e.getSource() == view.getBtnIniciarSesion()) {
			String user = view.getUserField().getText();
			String pass = view.getPasswordField().getText();
			
			if(model.autenticate(user, pass)) {
				view.setVisible(false);
				MenuView menuView = MenuView.getMenuView();
				MenuController menuController = new MenuController(menuView);
				menuView.setVisible(true);
			}
			
		}
		
	}
}
