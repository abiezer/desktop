package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import prestamo.desktop.model.UserModel;
import prestamo.desktop.view.LoanView;
import prestamo.desktop.view.MenuView;

public class MenuController implements ActionListener{
	
	private MenuView view;
	
	MenuController(MenuView view){
		this.view = view;
		view.getMntmSolicitar().addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==view.getMntmSolicitar()) {
			view.setVisible(false);
			LoanView lv = LoanView.getLoanView();
			
			lv.setVisible(true);
		}
		System.out.print(UserModel.getUserModel().getNombre());
	}

	
	
}
