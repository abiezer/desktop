package prestamo.desktop;

import static org.junit.Assert.*;

import org.junit.Test;

import prestamo.desktop.model.UserModel;

public class UserUpdateTest {

	@Test
	public void test() {
		UserModel us = UserModel.getUserModel();
		us.setId("1");
		us.setNombre("Pedro");
		us.setApellido("Maduro");
		us.setCorreo("pedro@hotmail.com");
		us.setTipo("1");
		us.setCalle("Picarte");
		us.setNumeroCasa("1945");
		us.setClave("321");
		us.setComunaId("1");
		assertTrue(us.update());
	}

}
